﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class IntroController : MonoBehaviour
{
    public GameObject Text_1;
    public GameObject Text_2;
    public GameObject Text_3;
    public GameObject Reference_1;
    public GameObject Reference_2;
    public GameObject Button_Next_1;
    public GameObject Button_Next_2;
    public GameObject Button_Prev_1;
    public GameObject Button_Prev_2;
    public GameObject Button_Start;
    public GameObject Button_Sound;
    public AudioSource AudioExample;

    public void Next_1()
    {
        Text_1.SetActive(false);
        Button_Next_1.SetActive(false);
        Reference_1.SetActive(false);
        Text_2.SetActive(true);
        Button_Next_2.SetActive(true);
        Reference_2.SetActive(true);
        Button_Prev_1.SetActive(true);
    }

    public void Next_2()
    {
        Text_2.SetActive(false);
        Button_Next_2.SetActive(false);
        Reference_2.SetActive(false);
        Button_Prev_1.SetActive(false);
        Text_3.SetActive(true);
        Button_Prev_2.SetActive(true);
        Button_Start.SetActive(true);
        Button_Sound.SetActive(true);
    }

    public void Prev_1()
    {
        Text_2.SetActive(false);
        Button_Next_2.SetActive(false);
        Reference_2.SetActive(false);
        Button_Prev_1.SetActive(false);
        Text_1.SetActive(true);
        Button_Next_1.SetActive(true);
        Reference_1.SetActive(true);
    }

    public void Prev_2()
    {
        Text_3.SetActive(false);
        Button_Prev_2.SetActive(false);
        Button_Start.SetActive(false);
        Button_Sound.SetActive(false);
        Text_2.SetActive(true);
        Button_Next_2.SetActive(true);
        Button_Prev_1.SetActive(true);
        Reference_2.SetActive(true);
    }

    public void PlaySound()
    {
        AudioExample.Play();
    }

    public void StartProject()  //Запуск основного проекта
    {
        SceneManager.LoadScene(1);
    }
}
