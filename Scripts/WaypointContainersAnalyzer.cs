﻿using System.Collections;
using UnityEngine;
using SWS;
using TurnTheGameOn.Timer;

[RequireComponent(typeof(AudioSource))]
public class WaypointContainersAnalyzer : MonoBehaviour 
{
    #region Values Description
    
	private splineMove2 myMove;
    private splineMove trainMove;//busMove;
    private splineMove bikeMove;
    public AudioSource BikeRingBell;
    //private splineMove busMove2;
	//private GameObject buscamera, buscamerain;
	//private HandsScript hands;
	private mqttTest mqttdata;
	private HttpDataScript httpdata;
	//private AudioSource describeTakeItem; 	
    //public AudioSource dogBarkingSound;
	private TakerScript sunglasses;
	private GameObject player, playercamera;
    private bool toReserveWay = false;
    public GameObject TrainCamera;
    public GameObject Escalator_1Camera;
    public GameObject Escalator_2Camera;
    private bool ChangeTrainRoute = false;
    //private GameObject InstructionsCamera;

	/* Mesh carts */
	//private GameObject meshcart, meshcartfull, meshcartdummy;

	/* Bus pay */
	//private OpenBusDoor1A busPayDoor;

	//private PhysicalTakerScript sunglassesPhysical, ballPhysical, bagPhysical, coatPhysical;
	//private DoorOpenerScript doorPhysical;
	private static GeneralConfig config;
    //public GameObject num_117;
    //public GameObject num_904;
    //public GameObject num_117_2;
    //public GameObject num_904_2;
    //public GameObject Warning;
    //public GameObject bus;
    //public bool way = false;
    //public bool dog = false;
    //public bool marketStop = false;
    //public bool busPay = false;
    //public bool busOutSignal = false;

    /* Playing audio */
    //public bool playingAudioDescribeTakeItem = false;	

	/* Bus audio */
	//public bool playingAudioDescribeBusStickHand = false;

	/* Played audio */
	public bool playedAudioDescribeTakeItem = false;

	/* Bus audio */
	//public bool playedAudioDescribeBusStickHand = false;
	//public bool comodothingstaken = false;
	//public bool coatthingstaken = false;
	public bool unlockingToMarket = false;
	public bool unlockingToHome = false;

	/* Physical items */
	//private bool nearTheCoat = false;
	//private bool coatIsTaken = false;

	/* Timers */
	//private GameObject timerBus;
    //private Timer timerIntoBus;

	/* Delay Shit */
	private bool calledToGoToDoor = false;
	public string handsExecLimitDirection = "";
	public int handsExecLimit = 0;
    private int tempStep = 0;
    #endregion

    #region voidStart
    void Start () 
	{        
        player = GameObject.Find ("FPSController");
		playercamera = GameObject.FindGameObjectWithTag ("MainCamera");
		myMove = GameObject.Find ("FPSController").GetComponent<splineMove2> ();
        bikeMove = GameObject.Find("BikeRider 2 1").GetComponent<splineMove>();
        //busMove = GameObject.Find ("EXTE_BUS_STRUCTURE_SepratePartsBUSBUS").GetComponent<splineMove> ();
        trainMove = GameObject.Find("Train").GetComponent<splineMove>();    //
        //busMove2 = GameObject.Find("EXTE_BUS_STRUCTURE_SepratePartsBUSBUS2").GetComponent<splineMove>();
        //buscamera = GameObject.FindGameObjectWithTag ("BusCamera");
        //buscamerain = GameObject.FindGameObjectWithTag("BusCameraIn");
        //hands = GameObject.Find ("FPSController").GetComponent<HandsScript> ();
        //InstructionsCamera = GameObject.FindGameObjectWithTag("InstructionsCamera");

		/* Audio */
		//describeTakeItem =  GameObject.Find ("TakeItem").GetComponent<AudioSource> ();		

		/* Mqtt Data */
		mqttdata = GameObject.Find ("FPSController").GetComponent<mqttTest>();
		//sunglasses = GameObject.Find ("mH_Sunglasses").GetComponent<TakerScript>();
		//doorPhysical = GameObject.FindGameObjectWithTag ("DoorHome").GetComponent<DoorOpenerScript>();
		config = new GeneralConfig ();

		/* Http Data */
		httpdata = GameObject.Find ("FPSController").GetComponent<HttpDataScript>();

		/* Mesh carts */
		//meshcart = GameObject.FindGameObjectWithTag ("MeshCartFPS");
		//meshcartfull = GameObject.FindGameObjectWithTag ("MeshCartFPSFull");
		//meshcartdummy = GameObject.FindGameObjectWithTag ("MeshCartDummy");

		// disable mesh cart views
		//meshcart.SetActive (false);
		//meshcartfull.SetActive (false);

		/* Physical items */
		//sunglassesPhysical = GameObject.Find ("mH_Sunglasses").GetComponent<PhysicalTakerScript>();
		//ballPhysical = GameObject.Find ("mH_Baseball").GetComponent<PhysicalTakerScript>();
		//bagPhysical = GameObject.Find ("Bag").GetComponent<PhysicalTakerScript>();
		//busPayDoor = GameObject.FindGameObjectWithTag ("BusDoor").GetComponent<OpenBusDoor1A>();

		/* Timers */

        // IntoBus
        //timerBus = GameObject.Find("TimerIntoBus");
        //timerIntoBus = GameObject.FindGameObjectWithTag("TimerBus").GetComponent<Timer>();

		myMove.setStartedPath ("ToStairs");//HomeToMarket1
        //hands.hideHands();
        myMove.ChangeSpeed(1);
    }
    #endregion

    // Home to Market Scenario

    #region HomeToMarketSounds
    public void containerHomeToMarketSoundsAnalyzer() 
	{
		
        /*if (myMove.currentPathContainer ().name.Equals ("HomeToMarket1")) 
		{
			// Turn hands off 
			if (myMove.currentPoint != config.WAYPOINT_COMODO && myMove.currentPoint != config.WAYPOINT_COAT) 
			{
				showHands(false);
			}
			// Возле комода
			if (myMove.currentPoint == config.WAYPOINT_COMODO) 
			{
				showHands(true); 	// Show hands
				setHandsExecLimit("down", config.WAYPOINT_COMODO_LEFT_HAND_TAKE_LIMIT);
				StartCoroutine(playDelayedDescibeComodoBag()); //  Describe delayed glasses take
				if (comodothingstaken) 
				{ 								
					showHands(false); 									// Hide hands
					// Show some UI complete mark
				} // end comodo things taken
			}				
			// Возле двери с курткой
			if (myMove.currentPoint == config.WAYPOINT_COAT) 
			{
				showHands (true);
				playSound ("describeCoatDoor"); 						// Describe coat take
				if (coatthingstaken) 
				{
					showHands(false); 									// Hide hands
					// Show some UI complete mark
				} // end coat things taken
			}
			if (myMove.currentPoint == config.WAYPOINT_DOOR) 
			{
				playSound ("describeDoor"); 						// Describe door
			}
			if (myMove.currentPoint == config.WAYPOINT_AFTER_DOOR) 
			{
				playSound ("describeToBusStop"); 						// Describe to bus stop
			}
		}*/
	}
    #endregion

    #region MarketstopToMarketSounds
    public void containerFromMarketStopToMarketSoundsAnalyzer() 
	{
		/*if (myMove.currentPathContainer ().name.Equals ("FromMarketStopToMarket")) 
		{ 		
			if (myMove.currentPoint == 29) 
			{
				playSound ("describeMeshCartToPacket"); 	// Describe
				StartCoroutine(podnemitePoruchenGo()); 		// Describe after
				StartCoroutine(payPokupkiGo()); // Describe pay
			}
		}*/
	}
    #endregion

    public void containerManualFromMyMove(PathManager pathContainer, int index)
    {
        ///////////////////////////////////////////////////////////////
        if (myMove.currentPathContainer().name.Equals("ToStairs"))
        {
            if (index == 9)
            {
                myMove.SetPath(WaypointManager.Paths["ToPark"]);
            }
        }
        if (myMove.currentPathContainer().name.Equals("ToPark"))
        {
            if (index == 2)
            {
                bikeMove.SetPath(WaypointManager.Paths["BikeRoute3"]);
                bikeMove.ChangeSpeed(3);
                bikeMove.StartMove();
                //BikeRingBell.Play();
                StartCoroutine(BikeBell());
            }
            if (index == 19)
            {
                myMove.SetPath(WaypointManager.Paths["OnBikeRoad"]);
            }
        }
        if (myMove.currentPathContainer().name.Equals("OnBikeRoad"))
        {
            if (index == 2)
            {
                bikeMove.StartMove();
                bikeMove.startPoint = 0;
            }
            if (index == 6)
            {
                StartCoroutine(BikeBell());
            }
            if (myMove.currentPoint == 6 || myMove.currentPoint == 7)
            {
                Debug.Log("Поворот на дорожку 1");         
            }
            if (myMove.currentPoint == 7 && toReserveWay == true)
            {
                myMove.SetPath(WaypointManager.Paths["1stGetOut"]);
                toReserveWay = false;
                mqttdata.button = 0;
            }
            if (index == 9)
            {
                StartCoroutine(BikeBell());
            }
            if (myMove.currentPoint == 9 || myMove.currentPoint == 10)
            {
                Debug.Log("Поворот на дорожку 2");
            }
            if (myMove.currentPoint == 10 && toReserveWay == true)
            {
                myMove.SetPath(WaypointManager.Paths["2ndGetOut"]);
                toReserveWay = false;
                mqttdata.button = 0;
            }
            if (index == 15)
            {
                bikeMove.ChangeSpeed(4);
                StartCoroutine(BikeBell());
            }
        }
        if (myMove.currentPathContainer().name.Equals("1stGetOut"))
        {
            if (myMove.currentPoint == 3)
            {
                bikeMove.SetPath(WaypointManager.Paths["BikeRoute_1_2"]);
                bikeMove.GoToWaypoint(7);
                bikeMove.ChangeSpeed(3);
            }
        }
        if (myMove.currentPathContainer().name.Equals("2ndGetOut"))
        {
            if (myMove.currentPoint == 3)
            {
                bikeMove.SetPath(WaypointManager.Paths["BikeRoute_1_2"]);
                bikeMove.GoToWaypoint(10);
                bikeMove.ChangeSpeed(3);
            }
        }
        if ((myMove.currentPathContainer().name.Equals("OnBikeRoad") && myMove.currentPoint == 24) ||
            (myMove.currentPathContainer().name.Equals("1stGetOut") && myMove.currentPoint == 15) ||
            (myMove.currentPathContainer().name.Equals("2ndGetOut") && myMove.currentPoint == 14))
        {
            myMove.SetPath(WaypointManager.Paths["ToCorner"]);
        }
        if (myMove.currentPathContainer().name.Equals("ToCorner"))
        {
            if (index == 7)
            {
                bikeMove.SetPath(WaypointManager.Paths["BikeRoute2"]);
                bikeMove.ChangeSpeed(2);
            }
            if (index == 10)
            {
                StartCoroutine(BikeBell());
            }
            if (index == 11 || index == 12)
            {
                Debug.Log("Сигнал");
            }
            if (index == 25)///
            {
                myMove.SetPath(WaypointManager.Paths["ToEscalator"]);   //Спуск по эскалатору
            }
        }
        if (myMove.currentPathContainer().name.Equals("ToEscalator"))
        {
            if (index == 22)
            {                
                myMove.SetPath(WaypointManager.Paths["Escalator"]);              
            }
        }
        if (myMove.currentPathContainer().name.Equals("Escalator"))
        {
            if (index == 0)
            {
                playercamera.SetActive(false);
                Escalator_1Camera.SetActive(true);
                myMove.ChangeSpeed(0.4f);
            }
            if (index == 1)
            {
                Escalator_1Camera.SetActive(false);
                playercamera.SetActive(true);                
                myMove.SetPath(WaypointManager.Paths["ToTrain"]);
                myMove.ChangeSpeed(1f);
            }
        }

        if (myMove.currentPathContainer().name.Equals("ToTrain"))
        {
            if (index == 1)
            {
                trainMove.StartMove();
            }
            if (index == 5)
            {
                myMove.Pause();
            }
            if (index == 6)
            {
                playercamera.SetActive(false);
                TrainCamera.SetActive(true);   
                trainMove.SetPath(WaypointManager.Paths["TrainRoute_2"]);
            }
        }

        if (myMove.currentPathContainer().name.Equals("ToEscalator_2"))
        {
            if (index == 7)
            {
                myMove.SetPath(WaypointManager.Paths["Escalator_2"]);
            }
        }

        if (myMove.currentPathContainer().name.Equals("Escalator_2"))
        {
            if (index == 0)
            {
                playercamera.SetActive(false);
                Escalator_2Camera.SetActive(true);
                myMove.ChangeSpeed(0.4f);
            }
            if (index == 1)
            {
                Escalator_2Camera.SetActive(false);
                playercamera.SetActive(true);
                myMove.SetPath(WaypointManager.Paths["ToTheStreet"]);
                myMove.ChangeSpeed(1f);
            }
        }
        if (myMove.currentPathContainer().name.Equals("ToTheStreet"))
        {
            if (index == 21)
            {
                myMove.SetPath(WaypointManager.Paths["ToClinic"]);
            }
        }




            ///////////////////////////////////////////////////////////////        
            ///Train controll
            ///



            // From Bath To Bus
            /*#region FromBathToBus
            if (pathContainer.name.Equals ("FromBathToBus")) 
            {			
                //hands.hideHands();
                //meshcartfull.SetActive(false);
                //meshcart.SetActive(false);
                if (index == 3)    
                {
                    busMove.startPoint = config.WAYPOINT_BUS_SUB_STOP;  //размещаем автобус на углу
                    busMove.StartMove();
                }
                if (index == 5)
                {
                    myMove.ChangeSpeed(0.7f);
                }
                if (index == 7)
                {
                    busMove.startPoint = config.WAYPOINT_BUS_SUB_STOP;  //размещаем автобус на углу
                    busMove.StartMove();
                }
                if (index == 13)
                {
                    busMove.startPoint = config.WAYPOINT_BUS_SUB_STOP;  //размещаем автобус на углу
                    busMove.StartMove();
                }
                if (index == 17)
                {
                    busMove.startPoint = config.WAYPOINT_BUS_SUB_STOP;  //размещаем автобус на углу
                    busMove.StartMove();
                }
                if (index == 18) 
                {
                    // Стопарим игрока, пока не подьедет автобус
                    myMove.Pause (0.0f);
                    myMove.moveLock = true;
                }
                if (index == 20)
                {
                    //busPay = false;
                    Debug.Log("Redirect Bus Flag");
                }
                // Stop IntoBus Timer
                if (index != 18 && index != 19 && busMove.currentPoint == config.WAYPOINT_BUS_HOME_STOP)
                {
                    //timerIntoBus.StopTimer();
                }
                if (myMove.moveLock == true)
                {
                    Debug.Log ("MoveLock is true, stop trademill");
                    mqttdata.sendMqtt ("stop_trademill");
                } 
            }
            #endregion*/

            // Home To Market 1
            #region HomeToMarket
            if (pathContainer.name.Equals ("HomeToMarket1")) 
		{
            //meshcartfull.SetActive(false);
            //meshcart.SetActive(false);
            if (index < 2) 
			{
				myMove.Resume ();
				Debug.Log ("auto move forward to comodo");
			}
			if (index >= 3) 
			{
				myMove.Pause (0.0f);
			}
			if (index == 3) 
			{
				myMove.ChangeSpeed (3);
			}
			if (index == 5) 
			{
				myMove.ChangeSpeed (2);
			}
			if (index == 13) 
			{
				myMove.Pause (0.0f);
				myMove.SetPath (WaypointManager.Paths["FromHomeToStairs"]);
				myMove.GoToWaypoint (0);
			}
		}
        #endregion

        // From Home To Stairs
        #region HomeToStairs
        if (pathContainer.name.Equals("FromHomeToStairs"))         
        {
            if (index == 0)
            {
                //hands.hideHands();
                showHands(false);
                //meshcartfull.SetActive(false);
                //meshcart.SetActive(false);
                myMove.ChangeSpeed(1.5f);
            }
            if (index == 7)
            {
                Debug.Log("End of Stairs");
                myMove.SetPath(WaypointManager.Paths["FromBathToBus"]);
                myMove.GoToWaypoint(0);
                myMove.ChangeSpeed(1.5f);
            }
        }
        #endregion

        // From Marketstop To Market
        #region FromMarketStopToMarket
        if (pathContainer.name.Equals ("FromMarketStopToMarket"))  
		{
            ///////////////////////////////////Добавить флаг об услышанной остановке
            if (index == 0) 
			{
				Debug.Log ("Reloading camera view");
                //buscamerain.SetActive(false);
				//buscamera.SetActive (false);
				playercamera.SetActive (true);
                myMove.ChangeSpeed(1.5f);
                showHands(false);   //убираем руки и корзину (при переходе по меню)
                //meshcartfull.SetActive(false);
                //meshcart.SetActive(false);
			}
			if (index >= 10 && index < 33) 
			{
				myMove.Pause ();
  
			}
			if (index == 10) 
			{
                //Stop Yard Timer
                showHands(false);
                //meshcartfull.SetActive(false);
			}
			if (index == 12) 
			{
                showHands(true);
				//meshcartdummy.SetActive (false);
				//meshcart.SetActive (true);
			}
			// Выбор продуктов
			if (index == 20) 
			{
				mqttdata.sendMqtt ("stop_trademill");
			}
			if (index == 21) 
			{
				//meshcart.SetActive (false);
				//meshcartfull.SetActive (true);
			}
			// Выбор продуктов 2
			if (index == 26) 
			{
				mqttdata.sendMqtt ("stop_trademill");
			}
			if (index == 33) 
			{
				myMove.Resume ();
			}
			// Оплата покупок
			if (index == 29) 
			{
				mqttdata.sendMqtt ("stop_trademill");
			}
			if (index == 29) 
			{
				myMove.moveLock = true;
			}
			if (index == 30) 
			{
				myMove.Resume ();
                showHands(false);
                //meshcart.SetActive(false);
                //meshcartfull.SetActive(false);
            }
			if (index == 39) 
			{
				myMove.Pause ();
			}
		}
        #endregion

        // From Bus To Home
        #region FromBusToHome
        /*if (pathContainer.name.Equals ("FromBusToHome")) 
		{
            //meshcart.SetActive(false);
            //meshcartfull.SetActive(false);
            if (index == 0) 
			{
				Debug.Log ("Reloading camera view");
				//buscamera.SetActive (false);
				playercamera.SetActive (true);
			}
			if (index >= 4) 
			{
				myMove.Pause ();
			}
			if (index == 4) 
			{
				mqttdata.sendMqtt ("start_trademill");
			}
		}
        /*if (pathContainer.name.Equals("To3Busstop"))
        {
            if (index == 1)
            {
                //Warning.SetActive(true);
            }
            if (index == 3)
            {
                busMove.startPoint = config.WAYPOINT_BUS_SUB_STOP;  //размещаем автобус на углу
                busMove.StartMove();
                //StartCoroutine(ComeBack());
            }
        }*/
        #endregion
    }

    //Звуки
    #region PlaySounds
    public void playSound(string sound) 
	{
		/*if (sound == "describeTakeItem") // Describe pay at market
		{ 
			//if (!describeTakeItem.isPlaying && !playingAudioDescribeTakeItem) 
			//{
				//describeTakeItem.Play();
				//playingAudioDescribeTakeItem = true;
                //playingAudioDescribeTakeItem = false;
            //}
		}	*/	
	}
    #endregion

    public void OnRoadController()
    {
        if (myMove.currentPathContainer().name.Equals("OnBikeRoad") && (myMove.currentPoint == 6 || myMove.currentPoint == 7) && (Input.GetMouseButtonDown(1) || mqttdata.button == 1))
        {
            toReserveWay = true;
            Debug.Log("Сигнал получен - уход на резервный путь 1");
        }
        if (myMove.currentPathContainer().name.Equals("OnBikeRoad") && (myMove.currentPoint == 9 || myMove.currentPoint == 10) && (Input.GetMouseButtonDown(1) || mqttdata.button == 1))
        {
            toReserveWay = true;
            Debug.Log("Сигнал получен - уход на резервный путь 2");
        }
        if (myMove.currentPathContainer().name.Equals("ToCorner") && (myMove.currentPoint == 11 || myMove.currentPoint == 12) && (Input.GetMouseButtonDown(1) || mqttdata.button == 1))
        {
            myMove.Pause();
            Debug.Log("Сигнал получен - Стоп на повороте после парка");
        }
        if (myMove.currentPathContainer().name.Equals("ToPark") && (myMove.currentPoint == 2 || myMove.currentPoint == 3 || myMove.currentPoint == 4) && (Input.GetMouseButtonDown(1) || mqttdata.button == 1))
        {
            myMove.Pause();
            Debug.Log("Сигнал получен - Стоп на повороте к парку");
        }

    }

    /* Delayed Audio Coroutines */
    #region DelaySounds
    IEnumerator playDelayedDescibeComodoBag()
	{
		yield return new WaitForSeconds(2);
		playSound ("describeComodoBag");
		StopCoroutine (playDelayedDescibeComodoBag());
	}
	IEnumerator playDelayedDescibeComodoGlasses()
	{
		yield return new WaitForSeconds(2);
		playSound ("describeComodoGlasses");
		StopCoroutine (playDelayedDescibeComodoGlasses());
	}		
	IEnumerator podnemitePoruchenGo()
	{
		yield return new WaitForSeconds(25);   
		playSound ("describeTakeMeshCartUp");
		StopCoroutine (podnemitePoruchenGo());
	}
	IEnumerator payPokupkiGo()
	{
		yield return new WaitForSeconds(30);
		playSound ("describePayAtMarket");
		StopCoroutine (payPokupkiGo());
	}
    #endregion

    #region ShowHands
    public void showHands(bool show) 
	{
		if (show)
        {
			//if (hands.handsShowed == false) 
			//{
				//hands.showHands ();
			//}
		} 
		else 
		{
			//if (hands.handsShowed == true) 
			//{
				//hands.hideHands ();
			//}
		}
	}
    #endregion

    // Step analyze and lock
    #region containerMoveStepAnalyzer
    public void containerMoveStepAnalyzer() 
	{
		//Debug.Log ("Container Move Step Analyzer Hello");
		if (mqttdata.step == 1 && tempStep == 0 && myMove.getMoveLock () == false) 
		{
			tempStep = 1;
			myMove.Resume();
		}
		if (mqttdata.step == 0 && tempStep == 1 && myMove.getMoveLock () == false) 
		{
			tempStep = 0;
		}
		if (mqttdata.step == 1 && myMove.getMoveLock () == true) 
		{
			playSound ("describeNotYourBusStop");
		}
	}
    #endregion

    #region HandsTakerAnalyzer
    public void HandsTakerAnalyzer() 
	{
		if (handsExecLimitDirection == "down" && (mqttdata.leftarmdegree >= handsExecLimit || mqttdata.rightarmdegree >= handsExecLimit)) 
		{
			// activate taker
			if (mqttdata.lefttaker == 1) 
			{
				sunglasses.takeleft ();
				Debug.Log ("Down - left take");
			}
			if (mqttdata.righttaker == 1) 
			{
				sunglasses.takeright ();
				Debug.Log ("Down - right take");
			}
		}
		if (handsExecLimitDirection == "up" && (mqttdata.leftarmdegree <= handsExecLimit || mqttdata.rightarmdegree <= handsExecLimit)) 
		{
			// activate taker
			if (mqttdata.lefttaker == 1) 
			{
				sunglasses.takeleft ();
				Debug.Log ("UP - left take");
			}
			if (mqttdata.righttaker == 1) 
			{
				sunglasses.takeright ();
				Debug.Log ("UP - right take");
			}
		}
	}
    #endregion

    #region setHandsExecLimit
    public void setHandsExecLimit(string direction, int value) 
	{
		handsExecLimit = value;
		handsExecLimitDirection = direction;
	}
    #endregion

    // BUS STOPS/LOCK
    #region containerBusStopsAnalyzer
    public void containerBusStopsAnalyzer()     
	{
		/*if(myMove.getStartedPath().Equals("HomeToSwimmingPool") && buscamera.activeSelf) 
		{
			// unlock go, if swimming pool stop
			if(busMove.currentPoint == config.WAYPOINT_BUS_POOL_STOP) 
			{
				myMove.setMoveLock (false);
			}
			// lock go, if supermarket stop
			// ignore steps
			if (busMove.currentPoint == config.WAYPOINT_BUS_MARKET_STOP) 
			{
				myMove.setMoveLock (true);
			}
		}*/
		if (myMove.getStartedPath ().Equals ("HomeToMarket1") /*&& buscamera.activeSelf*/) 
		{
			// lock go, if swimming pool stop
			// ignore steps
			/*if(busMove.currentPoint == config.WAYPOINT_BUS_POOL_STOP) 
			{
				myMove.setMoveLock (true);
			}*/
			// unlock go, if supermarket stop
			/*if (busMove.currentPoint == config.WAYPOINT_BUS_MARKET_STOP)
			{
                Debug.Log("Автобус достиг магазина");
                unlockingToMarket = true;
				Debug.Log ("We are on the market stop");
				myMove.setMoveLock (false);
				Debug.Log ("Unlocked move");
				busPayDoor.inbus = false;
				busPayDoor.cameraSetIntoBus = false;
				busPayDoor.payed = false;
				mqttdata.buspay = 0;
                busMove.Pause();
                //if (marketStop == true)
                //{
                    //Debug.Log("Выход");
                    //myMove.SetPath (WaypointManager.Paths["FromMarketStopToMarket"]); //Начало движения от автобуса в магазин
                    //myMove.GoToWaypoint (0);
                    //myMove.Resume ();
                //}
                //else
                //{
                    //Debug.Log("Не выход");
                    //StartCoroutine(DontExitTheBus());
                //}
                /* unlock to home then */
                /*unlockingToHome = true;
				Debug.Log ("Moved to point 0");
			}*/
			/*if (busMove.currentPoint == config.WAYPOINT_BUS_HOME_STOP && unlockingToHome == true) 
			{
				unlockingToHome = false;
				busPayDoor.inbus = false;
				busPayDoor.cameraSetIntoBus = false;
				busPayDoor.payed = false;
				myMove.SetPath (WaypointManager.Paths["FromBusToHome"]);
				myMove.GoToWaypoint (0);
				myMove.Resume ();
			}*/
		}
	}
    #endregion

    #region PhysicalTakerAnalyzer
    void PhysicalTakerAnalyzer() 
	{
		/*if (myMove.currentPathContainer ().name.Equals ("HomeToMarket1")) 
		{
			if (myMove.currentPoint == config.WAYPOINT_COMODO) 
			{
				// start timer once							
				if (mqttdata.ball == 1) 
				{
					ballPhysical.takethisitem ();
				}
				if (mqttdata.bag == 1) 
				{
					bagPhysical.takethisitem ();
					StartCoroutine(playDelayedDescibeComodoGlasses()); //  Describe delayed glasses take
				}
				if (mqttdata.glasses == 1) 
				{
					sunglassesPhysical.takethisitem ();
				}

				if (mqttdata.coat == 1) 
				{
					mqttdata.coat = 0;
				}

				if (mqttdata.door == 1) 
				{
					mqttdata.door = 0;
				}
			}
			if (myMove.currentPoint == config.WAYPOINT_COAT) 
			{
				nearTheCoat = true;
				if (mqttdata.coat == 1) 
				{
					coatPhysical.takethisitem ();
					coatIsTaken = true;
				}
				if (mqttdata.door == 1) 
				{
					mqttdata.door = 0;
				}
			}
			if (sunglassesPhysical.taken && bagPhysical.taken && nearTheCoat == false)
			{ 
				myMove.moveLock = false;
				myMove.Resume ();
			}
			if (coatIsTaken == true && myMove.currentPoint != 11) 
			{
				Debug.Log ("Still calling coat");
				// Only once!
				if (!calledToGoToDoor) 
				{
					StartCoroutine (continueToGoToDoor ());
				}
			}
			if (myMove.currentPoint == 10) 
			{
				myMove.Resume ();
			}
			if (mqttdata.door == 1 && doorPhysical.doorIsOpen == false) 
			{
				doorPhysical.openHomeDoor ();
				playSound ("describeOpenDoor");
				StartCoroutine(continueToGo());
			}
		}*/
		/*if (myMove.currentPathContainer ().name.Equals ("FromMarketStopToMarket")) 
		{
			if (myMove.currentPoint == 29) 
			{
				if (mqttdata.buspay == 1) 
				{
					//meshcartfull.SetActive (false);
                    showHands(false);
					playSound("describeTakeItem");
					myMove.Resume ();
				}
			}
		}*/
			
		/* Bus */
		//if ((mqttdata.buspay == 1 || Input.GetMouseButtonDown(1)) && busPayDoor.inbus && !busPayDoor.payed) // pay once
		//{
            /*if (busPay == false)
            {
                busPayDoor.payed = true;
                playSound("describeTakeItem");
                Debug.Log("I payed");
                mqttdata.buspay = 0;    
                buscamerain.SetActive(false);
                playercamera.SetActive(true);
                myMove.SetPath(WaypointManager.Paths["InsideTheBus"]);
                myMove.GoToWaypoint(0);
                myMove.ChangeSpeed(2);
            }*/
        //}
        /*if (myMove.currentPathContainer().name.Equals("InsideTheBus"))
        {
            if (myMove.currentPoint == 8)
            {
                playercamera.SetActive(false);
                //buscamera.SetActive(true);
            }
        }*/
	}
    #endregion

    /* Till door delay */
    IEnumerator continueToGoToDoor()
	{
		calledToGoToDoor = true;
		yield return new WaitForSeconds(12);
		myMove.Resume ();
		StopCoroutine (continueToGoToDoor());
	}
	IEnumerator continueToGo()
	{
		yield return new WaitForSeconds(1);
		myMove.Resume ();
		StopCoroutine (continueToGo());
	}
    /*IEnumerator ComeBack()
    {
        yield return new WaitForSeconds(3);
        myMove.SetPath(WaypointManager.Paths["FromBathToBus"]);
        myMove.GoToWaypoint(15);
        //Warning.SetActive(false);
    }*/
    IEnumerator DontExitTheBus()
    {
        yield return new WaitForSeconds(15);
        /*if (busMove.currentPoint == config.WAYPOINT_BUS_MARKET_STOP && marketStop == false)
        {
            Warning.SetActive(true);
        }*/
    }
    IEnumerator BikeBell()
    {
        BikeRingBell.Play();
        yield return new WaitForSeconds(1.1f);
        BikeRingBell.Play();
    }

    /*void BusSwitcher()
    {
        if (busMove.currentPoint > 2 && busMove.currentPoint < 15 && myMove.pathContainer.name == "FromBathToBus" && myMove.currentPoint == 18) //Заменяем автобус
        {
            num_117.SetActive(false);
            num_904.SetActive(true);
            num_117_2.SetActive(false);
            num_904_2.SetActive(true);
            busMove.startPoint = config.WAYPOINT_BUS_SUB_STOP;
            busMove.StartMove();
        }
        if (busMove.currentPoint == 0 && myMove.pathContainer.name == "FromBathToBus" && myMove.currentPoint == 19 && num_117.activeSelf == true)  //Блокируем возможность захода в ненужный автобус
        {
            myMove.GoToWaypoint(18);
        }
        if (busMove.currentPoint != 0 && myMove.currentPoint == 19)  //Блокируем возможность подъема в автобус при отсутствии такового
        {
            myMove.GoToWaypoint(18);
        }
    }*/

    /*void BusSpeedChanger()//поменять скорость при движении от бассейна
    {
        if (busMove.currentPoint == 0)
        {
            busMove.ChangeSpeed(15);
        }        
        if (busMove.currentPoint == 7)//скорость автобуса от бассейна до магазина
        {
            busMove.ChangeSpeed(6);
        }
        if (busMove.currentPoint == 18)
        {
            busMove.ChangeSpeed(6);
        }
    }*/

    /*public void MakeSignal()
    {
        if (way == false)
        {
            if (myMove.pathContainer.name == "FromBathToBus" && myMove.currentPoint >= 14 && myMove.currentPoint <= 18 && (Input.GetMouseButtonDown(1) || mqttdata.button == 1))
            {
                way = true;
                busMove.startPoint = config.WAYPOINT_BUS_SUB_STOP;
                busMove.StartMove();
                Debug.Log("Get signal");
                mqttdata.button = 0;
            }
        }
        if (myMove.pathContainer.name == "FromBathToBus" && myMove.currentPoint == 18 && way == false)
        {
            myMove.SetPath(WaypointManager.Paths["To3Busstop"]);
            myMove.GoToWaypoint(0);
            Debug.Log("Root changed");
        }
        if (myMove.pathContainer.name == "FromBathToBus" && myMove.currentPoint == 0)
        {
            busMove.startPoint = config.WAYPOINT_BUS_SUB_STOP;
            busMove.StartMove();
        }
        if (myMove.pathContainer.name == "FromBathToBus" && myMove.currentPoint >= 9 && myMove.currentPoint < 12 && (Input.GetMouseButtonDown(1) || mqttdata.button == 1))
        {
            Debug.Log("Get wrong signal");
            Warning.SetActive(true);
            mqttdata.button = 0;
        }
        if (myMove.pathContainer.name == "FromBathToBus" && myMove.currentPoint == 13 && Warning.activeSelf == true)
        {
            Warning.SetActive(false);
        }
        if (busMove.currentPoint >= 7 && busMove.currentPoint <= 11)
        {
            if (Input.GetMouseButtonDown(1) || mqttdata.button == 1)
            {
                marketStop = true;
                mqttdata.button = 0;
            }
        }
    }*/

    /*void DogBarking()
    {
        if (myMove.pathContainer.name == "FromBathToBus" && myMove.currentPoint == 18 && busMove.currentPoint == 19 && num_117.activeSelf == true)
        {
            dogBarkingSound.Play();
            Debug.Log("Dog Sound Play");
        }
        if (dogBarkingSound.isPlaying == true)
        {
            if (Input.GetMouseButtonDown(1) || mqttdata.button == 1)
            {
                Debug.Log("Dog checked");
                dog = true;
                mqttdata.button = 0;
            }
        }
    }*/

    /*void BusTimerController()
    {
        if (myMove.pathContainer.name == "FromBathToBus" && myMove.currentPoint == 18 && busMove.currentPoint == config.WAYPOINT_BUS_HOME_STOP && num_904.activeSelf == true)
        {
            timerBus.SetActive(true);            
            timerIntoBus.StartTimer();
        }
        if (myMove.pathContainer.name == "FromBathToBus" && myMove.currentPoint == 19)
        {
            timerIntoBus.StopTimer();
        }
    }*/

    void PauseAll()
    {
        if (Time.timeScale == 1)
        {
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;
        }
    }

   /* void ShowSignals()
    {
        if (way == true)
        {
            BusStop_Checked.SetActive(true);
        }
        else if (way == false)
        {
            BusStop_Checked.SetActive(false);
        }
        if (dog == true)
        {
            Dog_Checked.SetActive(true);
        }
        else if (dog == false)
        {
            Dog_Checked.SetActive(false);
        }
        if (marketStop == true)
        {
            MarkeStop_Checked.SetActive(true);
        }
        else if (marketStop == false)
        {
            MarkeStop_Checked.SetActive(false);
        }
    }*/

    // Update is called once per frame
    void Update () 
	{
        OnRoadController();
        containerHomeToMarketSoundsAnalyzer ();
		containerFromMarketStopToMarketSoundsAnalyzer ();
		containerMoveStepAnalyzer ();
		HandsTakerAnalyzer ();
		containerBusStopsAnalyzer();
        //PhysicalTakerAnalyzer ();
        //BusSwitcher();
        //BusSpeedChanger();
        //MakeSignal();
        //DogBarking();
        //ShowSignals();
        //BusTimerController();

        ///

        if (trainMove.currentPathContainer().name.Equals("TrainRoute_2") && trainMove.currentPoint == 23)
        {
            Debug.Log("TO2STATION");
            //trainMove.SetPath(WaypointManager.Paths["TrainRoute_3"]);
            trainMove.SetPath(WaypointManager.Paths["TrainSubRoute_1"]);
        }

        if (trainMove.currentPathContainer().name.Equals("TrainSubRoute_1") && trainMove.currentPoint == 20)
        {
            trainMove.SetPath(WaypointManager.Paths["TrainSubRoute_2"]);
            trainMove.currentPoint = 0;
        }
        if (trainMove.currentPathContainer().name.Equals("TrainSubRoute_2") && trainMove.currentPoint == 20)
        {
            trainMove.SetPath(WaypointManager.Paths["TrainRoute_3"]);
        }

        if (trainMove.currentPathContainer().name.Equals("TrainRoute_3") && trainMove.currentPoint == 14 && TrainCamera.activeSelf == true)
        {
            playercamera.SetActive(true);
            TrainCamera.SetActive(false);
            myMove.SetPath(WaypointManager.Paths["ToEscalator_2"]);
        }

        if (Input.GetKeyDown(KeyCode.Pause) || Input.GetKeyDown(KeyCode.P))
        {
            PauseAll();
        }
    }
}